<?php namespace Tests\algorithm\leetcode;

use Closure;
use Exception;
use Tests\TestCase;

/**
 * Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.
 * Symbol       Value
 * I             1
 * V             5
 * X             10
 * L             50
 * C             100
 * D             500
 * M             1000
 * For example, 2 is written as II in Roman numeral, just two one's added together. 12 is written as XII, which is simply X + II. The number 27 is written as XXVII, which is XX + V + II.
 *
 * Roman numerals are usually written largest to smallest from left to right. However, the numeral for four is not IIII. Instead, the number four is written as IV. Because the one is before the five we subtract it making four. The same principle applies to the number nine, which is written as IX. There are six instances where subtraction is used:
 *
 * I can be placed before V (5) and X (10) to make 4 and 9.
 * X can be placed before L (50) and C (100) to make 40 and 90.
 * C can be placed before D (500) and M (1000) to make 400 and 900.
 * Given an integer, convert it to a roman numeral.
 * 1 <= num <= 3999
 */
class Problem12 extends TestCase
{
	/**
	 * Input: num = 3
	 * Output: "III"
	 * Explanation: 3 is represented as 3 ones.
	 */
	private function case_1(Closure $solution)
	{
		$input = 3;
		$output = $solution($input);
		$this->assertEquals("III", $output);
	}

	/**
	 * Input: num = 58
	 * Output: "LVIII"
	 * Explanation: L = 50, V = 5, III = 3.
	 */
	private function case_2(Closure $solution)
	{
		$input = 58;
		$output = $solution($input);
		$this->assertEquals("LVIII", $output);
	}

	/**
	 * Input: num = 1994
	 * Output: "MCMXCIV"
	 * Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.
	 */
	private function case_3(Closure $solution)
	{
		$input = 1994;
		$output = $solution($input);
		$this->assertEquals("MCMXCIV", $output);
	}

	public function test_run_solution(){
		$closure = function (int $input) {
			return $this->solution($input);
		};

		$this->case_1($closure);
		$this->case_2($closure);
		$this->case_3($closure);
	}

	private function solution(int $input): string
	{
		//たまに表を参照するのはいいと思う
		$list = [
			'digits' => [
				0 => '',
				1 => 'I',
				2 => 'II',
				3 => 'III',
				4 => 'IV',
				5 => 'V',
				6 => 'VI',
				7 => 'VII',
				8 => 'VIII',
				9 => 'IX',
			],
			'tens' => [
				0 => '',
				1 => 'X',
				2 => 'XX',
				3 => 'XXX',
				4 => 'XL',
				5 => 'L',
				6 => 'LX',
				7 => 'LXX',
				8 => 'LXXX',
				9 => 'XC',
			],
			'hundreds' => [
				0 => '',
				1 => 'C',
				2 => 'CC',
				3 => 'CCC',
				4 => 'CD',
				5 => 'D',
				6 => 'DC',
				7 => 'DCC',
				8 => 'DCCC',
				9 => 'CM',
			],
			'thousands' => [
				0 => '',
				1 => 'M',
				2 => 'MM',
				3 => 'MMM',
			]
		];

		list($thousands, $hundreds, $tens, $digits) = $this->getTHTD($input);
		return $list['thousands'][$thousands] .
				$list['hundreds'][$hundreds] .
				$list['tens'][$tens] .
				$list['digits'][$digits];
	}

	private function copy(string $char, int $amount)
	{
		$result = '';

		for($i=0; $i<$amount; $i++) {
			$result = $result . $char;
		}

		return $result;
	}

	private function getTHTD(string $integer)
	{
		$maxLen = 4;
		$len = strlen($integer);
		$integer = $this->copy('0', $maxLen-$len) . $integer;

		return [
			$thousands = substr($integer, 0, 1),
			$hundreds = substr($integer, 1, 1),
			$tens = substr($integer, 2, 1),
			$digits = substr($integer, 3, 1)
		];
	}

}
