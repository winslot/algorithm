<?php namespace Tests\algorithm\leetcode;

use Closure;
use Exception;
use Tests\TestCase;

/**
 * Class Problem2: Add Two Numbers
 *
 * You are given two non-empty linked lists representing two non-negative integers.
 * The digits are stored in reverse order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.
 *
 * You may assume the two numbers do not contain any leading zero, except the number 0 itself.
 */
class Problem2 extends TestCase
{
	/**
	 * Test Case
	 * Example:
	 * 		Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
	 * 		Output: 7 -> 0 -> 8
	 * 		Explanation: 342 + 465 = 807.
	 * @param closure $solution
	 * @throws Exception
	 */
	private function case_1(closure $solution)
	{
		$nodeListA = new ListNode(2);
		$nodeListA->next = new ListNode(4, new ListNode(3));

		$nodeListB = new ListNode(5);
		$nodeListB->next = new ListNode(6, new ListNode(4));

		$output = $solution($nodeListA, $nodeListB);

		$this->assertTrue($output instanceof ListNode);

		if (!$output instanceof ListNode) {
			throw new Exception("答えのクラスタイプが合致してません。");
		}

		$this->assertEquals(7, $output->val);
		$this->assertEquals(0, $output->next->val);
		$this->assertEquals(8, $output->next->next->val);
	}

	/**
	 * intに戻して計算する方法はできますが、上限を超えるテストケースは出るそうので、数字単位で計算していく。
	 * @param ListNode $listNodeA
	 * @param ListNode $listNodeB
	 * @return ListNode
	 * @throws Exception
	 */
	private function solution(ListNode $listNodeA, ListNode $listNodeB)
	{
		$currentNodeA = $listNodeA;
		$currentNodeB = $listNodeB;
		$outputNode = null;
		$currentOutputNode = null;
		$carryFlag = false;

		while(true) {
			//計算できる数字がない場合処理を終わらせる。
			if ($currentNodeA === null && $currentNodeB === null) {
				if ($carryFlag) {
					/** @var ListNode $currentOutputNode */
					$currentOutputNode->next = new ListNode(1);
				}
				break;
			}

			//数値を取得して合計を計算する。
			$digitA = $currentNodeA === null ? 0 : $currentNodeA->val;
			$digitB = $currentNodeB === null ? 0 : $currentNodeB->val;
			$sum = $digitA + $digitB;
			$sum = $carryFlag ? $sum+1 : $sum;

			//キャリーを更新する、single digitを算出する
			$carryFlag = $sum >= 10 ? true:false;
			$singleDigitNode = new ListNode($sum % 10);

			if ($outputNode === null) {
				$outputNode = $singleDigitNode;
			} else {
				/** @var ListNode $currentOutputNode */
				$currentOutputNode->next = $singleDigitNode;
			}

			//次の数字に移動させる。
			$currentNodeA = $currentNodeA === null ? $currentNodeA : $currentNodeA->next;
			$currentNodeB = $currentNodeB === null ? $currentNodeB : $currentNodeB->next;
			$currentOutputNode = $singleDigitNode;
		}

		return $outputNode;
	}

	/**
	 * @throws Exception
	 */
	public function test_run_solution(){
		$closure = function (ListNode $listNodeA, ListNode $listNodeB) {
			return $this->solution($listNodeA, $listNodeB);
		};

		$this->case_1($closure);
	}
}

/**
 * 公式が提供したノードクラス
 * Class ListNode
 */
class ListNode {
	public $val = 0;
	public $next = null;
	function __construct($val = 0, $next = null) {
		$this->val = $val;
		$this->next = $next;
	}
}
