<?php namespace Tests\algorithm\leetcode;

use Closure;
use Exception;
use Tests\TestCase;

/**
 * Class Problem: TwoSum
 *
 * Given an array of integers, return indices of the two numbers such that they add up to a specific target.
 * You may assume that each input would have exactly one solution, and you may not use the same element twice.
 */
class Problem1 extends TestCase
{
	/**
	 * Test Case
	 * Example:
	 * 		Given nums = [2, 7, 11, 15], target = 9,
	 *		Because nums[0] + nums[1] = 2 + 7 = 9,
	 *		return [0, 1].
	 * @param closure $solution
	 */
	private function case_1(closure $solution)
	{
		$numbers = [2, 7, 11, 15];
		$target = 9;

		$output = $solution($numbers, $target);
		$this->assertTrue(is_array($output));
		$this->assertTrue(count($output) === 2);
		$this->assertEquals(0, $output[0]);
		$this->assertEquals(1, $output[1]);
	}


	/**
	 * 時間の制限がないため、直接ダブルfor loopで対応する、最悪のパタンは O(2N)
	 * @param array $numbers
	 * @param int $target
	 * @return array
	 * @throws Exception
	 */
	private function solution(array $numbers, int $target)
	{
		$length = count($numbers);
		for ($i=0; $i<$length; $i++) {
			for ($j=0; $j<$length; $j++) {
				//自分と計算することを防止する
				if ($i === $j) {
					continue;
				}

				if ($numbers[$i] + $numbers[$j] === $target) {
					return [$i, $j];
				}
			}
		}

		throw new Exception("期待値と合致する数値がありませんでした。");
	}

	/**
	 * @throws Exception
	 */
	public function test_run_solution(){
		$closure = function ($numbers, $target) {
			return $this->solution($numbers, $target);
		};

		$this->case_1($closure);
	}
}
