<?php namespace Tests\algorithm\leetcode;

use Closure;
use Exception;
use Tests\TestCase;

/**
 * Class Problem8:  String to Integer (atoi)
 *
 * Implement atoi which converts a string to an integer.
 *
 * The function first discards as many whitespace characters as necessary until the first non-whitespace character is found.
 * Then, starting from this character, takes an optional initial plus or minus sign followed by as many numerical digits as possible,
 * and interprets them as a numerical value.
 *
 * The string can contain additional characters after those that form the integral number,
 * which are ignored and have no effect on the behavior of this function.
 *
 * If the first sequence of non-whitespace characters in str is not a valid integral number,
 * or if no such sequence exists because either str is empty or it contains only whitespace characters,
 * no conversion is performed.
 */
class Problem8 extends TestCase
{
	/**
	 * Test Case
	 * Example:
	 * 		Input: "42"
	 * 		Output: 42
	 * @param closure $solution
	 */
	private function case_1(closure $solution)
	{
		$inputString = "42";
		$output = $solution($inputString);

		$this->assertTrue($output === 42);
	}

	/**
	 * Test Case
	 * Example:
	 * 		Input: "   -42"
	 * 		Output: -42
	 * Explanation: The first non-whitespace character is '-', which is the minus sign.
	 * 				Then take as many numerical digits as possible, which gets 42.
	 * @param closure $solution
	 */
	private function case_2(closure $solution)
	{
		$inputString = "   -42";
		$output = $solution($inputString);

		$this->assertTrue($output === -42);
	}

	/**
	 * Test Case
	 * Example:
	 * 		Input: "4193 with words"
	 * 		Output: 4193
	 * Explanation: Conversion stops at digit '3' as the next character is not a numerical digit.
	 * @param closure $solution
	 */
	private function case_3(closure $solution)
	{
		$inputString = "4193 with words";
		$output = $solution($inputString);

		$this->assertTrue($output === 4193);
	}

	/**
	 * Test Case
	 * Example:
	 * 		Input: "words and 987"
	 * 		Output: 0
	 * Explanation: The first non-whitespace character is 'w', which is not a numerical
	 * 				digit or a +/- sign. Therefore no valid conversion could be performed.
	 * @param closure $solution
	 */
	private function case_4(closure $solution)
	{
		$inputString = "words and 987";
		$output = $solution($inputString);

		$this->assertTrue($output === 0);
	}

	/**
	 * Test Case
	 * Example:
	 * 		Input: "-91283472332"
	 * 		Output: -2147483648
	 * Explanation: The number "-91283472332" is out of the range of a 32-bit signed integer.
	 * 				Thefore INT_MIN (−231) is returned.
	 * @param closure $solution
	 */
	private function case_5(closure $solution)
	{
		$inputString = "-91283472332";
		$output = $solution($inputString);

		$this->assertTrue($output === -2147483648);
	}

	private function solution(string $input)
	{
		$input = trim($input);

		$result = "";

		if ($this->containRegexp($input[0], "/[0-9]/")) {
			$result = $this->findInteger($input);
			return $this->calcLimit(intval($result));
		}


		if ($input[0] == '-' || $input[0] == '+') {
			$result = $this->findInteger(
				substr($input,1)
			);
			return $this->calcLimit(intval("$input[0]" . $result));
		}

		return 0;
	}

	/**
	 * 先頭の数字を探す
	 * @param string $string
	 * @return string
	 */
	private function findInteger(string $string)
	{
		$result = "";

		for ($i=0; $i < strlen($string); $i++) {
			if (!$this->containRegexp($string[$i], "/[0-9]/")) {
				return $result;
			}

			$result = $result . $string[$i];
		}

		return $result;
	}

	/**
	 * 制限を超えないようにする
	 * 2の31-1 ~ -2の31
	 * @param int $integer
	 * @return int
	 */
	private function calcLimit(int $integer)
	{
		return max(
			min(intval($integer), pow(2,31) - 1),
			- pow(2,31)
		);
	}

	/**
	 * 正規表現の文字が入ってるかどうか
	 * @param string $string
	 * @param string $regexp
	 * @return bool
	 */
	private function containRegexp(string $string, string $regexp):bool
	{
		return preg_match($regexp, $string);
	}

	/**
	 * @throws Exception
	 */
	public function test_run_solution(){
		$closure = function (string $input) {
			return $this->solution($input);
		};

		$this->case_1($closure);
		$this->case_2($closure);
		$this->case_3($closure);
		$this->case_4($closure);
		$this->case_5($closure);
	}
}
