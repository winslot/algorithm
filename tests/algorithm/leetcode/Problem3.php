<?php namespace Tests\algorithm\leetcode;

use Closure;
use Exception;
use Tests\TestCase;

/**
 * Class Problem3:  Longest Substring Without Repeating Characters
 *
 * Given a string, find the length of the longest substring without repeating characters.
 */
class Problem3 extends TestCase
{
	/**
	 * Test Case
	 * Example:
	 * 		Input: "abcabcbb"
	 *		Output: 3
	 *		Explanation: The answer is "abc", with the length of 3.
	 * @param closure $solution
	 */
	private function case_1(closure $solution)
	{
		$input = "abcabcbb";
		$output = $solution($input);

		$this->assertTrue(is_integer($output));
		$this->assertEquals(3, $output);
	}

	/**
	 * Test Case
	 * Example:
	 * 		Input: "bbbbb"
	 *		Output: 1
	 *		Explanation: The answer is "b", with the length of 1.
	 * @param closure $solution
	 */
	private function case_2(closure $solution)
	{
		$input = "bbbbb";
		$output = $solution($input);

		$this->assertTrue(is_integer($output));
		$this->assertEquals(1, $output);
	}

	/**
	 * Test Case
	 * Example:
	 * 		Input: "pwwkew"
	 *		Output: 3
	 *		Explanation: The answer is "wke", with the length of 3.  Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
	 * @param closure $solution
	 */
	private function case_3(closure $solution)
	{
		$input = "pwwkew";
		$output = $solution($input);

		$this->assertTrue(is_integer($output));
		$this->assertEquals(3, $output);
	}

	/**
	 * キャラクター毎に重複までの長さを計算させる、ストリングの長さが残り数より大きいの場合直接結果をリターンする
	 * @param string $input
	 * @return int
	 * @throws Exception
	 */
	private function solution(string $input)
	{
		$length = strlen($input);
		$result = 0;

		for ($i=0; $i<$length; $i++) {
			//残りストリングより大きい場合、早期リターンする。
			if($length - $i < $result) {
				return $result;
			}

			//重複までの長さを計算する、ストリングは少しずつ消されていく
			$string = substr($input, $i);
			$result = max($result, $this->calcLongestSubstringWithoutRepeatingCharacters($string));
		}

		return $result;
	}

	/**
	 * 毎に重複までの長さを計算する
	 * @param String $input
	 * @return int
	 */
	private function calcLongestSubstringWithoutRepeatingCharacters(String $input)
	{
		$length = strlen($input);
		$storageSpace = array();

		for ($i=0; $i<$length; $i++) {
			$character = substr($input, $i, 1);
			$result = array_search($character, $storageSpace);

			if ($result !== false) {
				return count($storageSpace);
			}

			array_push($storageSpace, $character);
		}

		return count($storageSpace);
	}

	/**
	 * @throws Exception
	 */
	public function test_run_solution(){
		$closure = function (string $input) {
			return $this->solution($input);
		};

		$this->case_1($closure);
		$this->case_2($closure);
		$this->case_3($closure);
	}
}
