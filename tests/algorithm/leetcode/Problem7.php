<?php namespace Tests\algorithm\leetcode;

use Closure;
use Exception;
use Tests\TestCase;

/**
 * Class Problem7:  Reverse Integer
 *
 * Given a 32-bit signed integer, reverse digits of an integer.
 *
 * Assume we are dealing with an environment which could only store integers within the 32-bit signed integer range: [−231,  231 − 1].
 * For the purpose of this problem, assume that your function returns 0 when the reversed integer overflows.
 */
class Problem7 extends TestCase
{
	/**
	 * Test Case
	 * Example:
	 * 		Input: 123
	 * 		Output: 321
	 * @param closure $solution
	 */
	private function case_1(closure $solution)
	{
		$inputString = 123;
		$output = $solution($inputString);

		$this->assertTrue($output === 321);
	}

	/**
	 * Test Case
	 * Example:
	 * 		Input: -123
	 * 		Output: -321
	 * @param closure $solution
	 */
	private function case_2(closure $solution)
	{
		$inputString = -123;
		$output = $solution($inputString);

		$this->assertTrue($output === -321);
	}

	/**
	 * Test Case
	 * Example:
	 * 		Input: 120
	 * 		Output: 21
	 * @param closure $solution
	 */
	private function case_3(closure $solution)
	{
		$inputString = 120;
		$output = $solution($inputString);

		$this->assertTrue($output === 21);
	}


	private function solution(int $input)
	{
		$stringNumber = strval($input);

		$result = 0;

		if ($stringNumber[0] === '-') {
			$result = intval('-' . strrev(substr($stringNumber, 1)));
		} else {
			$result = intval(
				strrev($stringNumber)
			);
		}

		//2の31-1 ~ -2の31
		$max = pow(2,31) - 1;
		$min = - pow(2,31);

		if ($result > $max || $result < $min) {
			return 0;
		}

		return $result;
	}

	/**
	 * @throws Exception
	 */
	public function test_run_solution(){
		$closure = function (string $input) {
			return $this->solution($input);
		};

		$this->case_1($closure);
		$this->case_2($closure);
		$this->case_3($closure);
	}
}
