<?php namespace Tests\algorithm\leetcode;

use Closure;
use Exception;
use Tests\TestCase;

/**
 * Class Problem10:  Regular Expression Matching
 *
 * Given an input string (s) and a pattern (p), implement regular expression matching with support for '.' and '*'.
 *
 * '.' Matches any single character.
 * '*' Matches zero or more of the preceding element.
 *
 * The matching should cover the entire input string (not partial).
 *
 * ・ s could be empty and contains only lowercase letters a-z.
 * ・ p could be empty and contains only lowercase letters a-z, and characters like . or *.
 */
class Problem10_2 extends TestCase
{
	/**
	 * Test Case
	 * Example:
	 * 		Input:	s = "aa"
	 *				p = "a"
	 * 		Output: false
	 * Explanation: "a" does not match the entire string "aa".
	 * @param Closure $solution
	 */
	private function case_1(Closure $solution)
	{
		$string = "aa";
		$patten = "a";

		$output = $solution($string, $patten);

		$this->assertFalse($output);
	}

	/**
	 * Test Case
	 * Example:
	 * 		Input:	s = "aa"
	 * 				p = "a*"
	 * 		Output: true
	 * Explanation: '*' means zero or more of the preceding element, 'a'.
	 * 				Therefore, by repeating 'a' once, it becomes "aa".
	 * @param Closure $solution
	 */
	private function case_2(Closure $solution)
	{
		$string = "aa";
		$patten = "a*";

		$output = $solution($string, $patten);

		$this->assertTrue($output);
	}

	/**
	 * Test Case
	 * Example:
	 * 		Input:	s = "ab"
	 * 				p = ".*"
	 * 		Output: true
	 * Explanation: ".*" means "zero or more (*) of any character (.)".
	 * @param Closure $solution
	 */
	private function case_3(Closure $solution)
	{
		$string = "ab";
		$patten = ".*";

		$output = $solution($string, $patten);

		$this->assertTrue($output);
	}

	/**
	 * Test Case
	 * Example:
	 * 		Input:	s = "aab"
	 * 				p = "c*a*b"
	 * 		Output: true
	 * Explanation: c can be repeated 0 times, a can be repeated 1 time. Therefore, it matches "aab".
	 * @param Closure $solution
	 */
	private function case_4(Closure $solution)
	{
		$string = "aab";
		$patten = "c*a*b";

		$output = $solution($string, $patten);

		$this->assertTrue($output);
	}

	// s = "mississippi"
	// p = "mis*is*p*."

	/**
	 * Test Case
	 * Example:
	 * 		Input:	s = "mississippi"
	 * 				p = "mis*is*p*."
	 * 		Output: false
	 * @param Closure $solution
	 */
	private function case_5(Closure $solution)
	{
		$string = "mississippi";
		$patten = "mis*is*p*.";
		$output = $solution($string, $patten);

		$this->assertFalse($output);
	}

	/**
	 * @throws Exception
	 */
	public function test_run_solution(){
		$closure = function (string $input, string $patten) {
			return $this->solution($input, $patten);
		};

		$this->case_1($closure);
		$this->case_2($closure);
		$this->case_3($closure);
		$this->case_4($closure);
		$this->case_5($closure);
		$this->freeCase($closure);
	}
	private function freeCase(Closure $solution)
	{
		$string = "ab";
		$patten = ".*";

		$output = $solution($string, $patten);

		$this->assertTrue($output);
	}


	private function solution(string $string, string $patten)
	{
		return $this->match($patten, $string);
	}

	private function match(string $patten, string $string)
	{
		if(strlen($patten)==0 && strlen($string)==0) {
			return true;
		}

		if(strlen($patten)==0 && strlen($string)!=0) {
			return false;
		}

		list($cmd, $otherCmds) = $this->cutOneComand($patten);

		//TODO 残りコマンド0
		$isSingle = strlen($cmd) == 1;
		if(!$isSingle) {
			$keyword = $cmd[0]; //m*のm

			//string:sssa cmd:s*なら、sssa ssa sa a全部残りのコマンドとまっちしてみる
			$chooseCount = $this->chooseAmount($string, $cmd[0]);

			for($i=0; $i < $chooseCount; $i++) {
				$result = $this->solution(
					substr($string, $i),
					 $otherCmds
				);

				if($result) {
					return $result;
				}
			}

			return false;
		}


		$stringLength = strlen($string);
		//コマンド残ってるが、string残ってない
		if($stringLength < 1) {
			return false;
		}

		//.
		if($cmd =='.') {
			return $this->solution(
				substr($string, 1), $otherCmds);
		}

		//m
		return $cmd == $string[0] ? $this->solution(substr($string, 1), $otherCmds) : false;
	}


	/**
	 * input:
	 *  string: sssa
	 *  keyword: s
	 * output:
	 *  int 4
	 *  [sssa, ssa, sa, a]
	 */
	private function chooseAmount(string $string, string $keyword)
	{
		$result = 1;
		$length = strlen($string);

		//.
		if ($keyword == '.') {
			return $length+1; // 0も含める
		}

		for($i = 0; $i<$length; $i++) {
			if($string[$i] != $keyword) {
				return $result;
			}

			$result++;
		}

		return $result;
	}

	/**
	 * input "mis*is*p*."
	 * output ['m', 'is*is*p*.']
	 */
	private function cutOneComand($cmds)
	{
		$shouldCutOneChar =  isset($cmds[1]) ? $cmds[1] != "*" : true;
		if ($shouldCutOneChar) {
			//['m', 'is*is*p*.']
			return [$cmds[0], substr($cmds, 1)];
		}



		//['m*', 'is*is*p*.']
		return [
			$cmds[0] . $cmds[1],
			substr($cmds, 2)
		];
	}

}
