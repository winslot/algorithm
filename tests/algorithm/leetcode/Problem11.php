<?php namespace Tests\algorithm\leetcode;

use Closure;
use Exception;
use Tests\TestCase;

/**
* You are given an integer array height of length n. There are n vertical lines drawn such that the two endpoints of the ith line are (i, 0) and (i, height[i]).
* Find two lines that together with the x-axis form a container, such that the container contains the most water.
* Return the maximum amount of water a container can store.
* Notice that you may not slant the container.
 */
class Problem11 extends TestCase
{
	/**
	 * Example:
	 *	 	Input: height = [1,8,6,2,5,4,8,3,7]
	 * 		Output: 49
	 * Explanation: The above vertical lines are represented by array [1,8,6,2,5,4,8,3,7]. In this case, the max area of water (blue section) the container can contain is 49.
	 */
	private function case_1(Closure $solution)
	{
		$input = [1,8,6,2,5,4,8,3,7];
		$output = $solution($input);
		$this->assertEquals(49, $output);
	}

	/**
	 * Test Case
	 * Example:
	 * 		Input: height = [1,1]
	 * 		Output: 1
	 * @param Closure $solution
	 */
	private function case_2(Closure $solution)
	{
		$input = [1,1];
		$output = $solution($input);
		$this->assertEquals(1, $output);
	}

	/**
	 * @param string $string
	 * @param string $patten
	 * @return bool
	 * @throws Exception
	 */
	private function solution(array $input): int
	{
		$resultHight = 0;
		$resultDistance = 0;
		$result = 0;

		$len = count($input);
		//$input[$i] => hight1
		//$input[$j] => hight2
		//$i-$j = distance
		for($i=0; $i < $len; $i++) {
			for($j=$i+1; $j<$len; $j++) {
				//TODO 残り僅かの場合、半分チェックだけでいい
				//節約の方法を考える
				//そう早期リターンの条件を考える
				$minHight = min($input[$i], $input[$j]);
				$distance = $j - $i;
				if($minHight < $resultHight && $distance < $resultDistance) {
					continue;
				}

				$innerProduct = $minHight * $distance;
				if($innerProduct > $result) {
					$result = $innerProduct;
					$resultHight = $minHight;
					$resultDistance = $distance;
				}
			}
		}

		return $result;
	}

	/**
	 * @throws Exception
	 */
	public function test_run_solution(){
		$closure = function (array $input) {
			return $this->solution($input);
		};

		$this->case_1($closure);
		$this->case_2($closure);
	}
}
