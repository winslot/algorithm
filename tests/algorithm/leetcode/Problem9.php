<?php namespace Tests\algorithm\leetcode;

use Closure;
use Exception;
use Tests\TestCase;

/**
 * Class Problem9:  Palindrome Number
 *
 * Determine whether an integer is a palindrome. An integer is a palindrome when it reads the same backward as forward.
 */
class Problem9 extends TestCase
{
	/**
	 * Test Case
	 * Example:
	 * 		Input: 121
	 * 		Output: true
	 * @param closure $solution
	 */
	private function case_1(closure $solution)
	{
		$input = 121;
		$output = $solution($input);

		$this->assertTrue($output);
	}

	/**
	 * Test Case
	 * Example:
	 * 		Input: -121
	 * 		Output: false
	 * Explanation: From left to right, it reads -121. From right to left, it becomes 121-. Therefore it is not a palindrome.
	 * @param closure $solution
	 */
	private function case_2(closure $solution)
	{
		$inputString = "-121";
		$output = $solution($inputString);

		$this->assertFalse($output);
	}

	/**
	 * Test Case
	 * Example:
	 * 		Input: 10
	 * 		Output: false
	 * Explanation: Reads 01 from right to left. Therefore it is not a palindrome.
	 * @param closure $solution
	 */
	private function case_3(closure $solution)
	{
		$inputString = 10;
		$output = $solution($inputString);

		$this->assertFalse($output);
	}


	/**
	 * Problem5とほぼ同じ内容、そのままコピーするw
	 * @param int $input
	 * @return string
	 * @throws Exception
	 */
	private function solution(int $input)
	{
		if ($input < 0) {
			return false;
		}

		return $this->isPalindromic(strval($input));
	}

	/**
	 * 回文かどうか
	 * @param string $string
	 * @return bool
	 * @throws Exception
	 */
	function isPalindromic(string $string)
	{
		return strrev($string) === $string;
	}

	/**
	 * @throws Exception
	 */
	public function test_run_solution(){
		$closure = function (string $input) {
			return $this->solution($input);
		};

		$this->case_1($closure);
		$this->case_2($closure);
		$this->case_3($closure);
	}
}
