<?php namespace Tests\algorithm\leetcode;

use Closure;
use Exception;
use Tests\TestCase;

/**
 * Class Median of Two Sorted Arrays
 *
 * There are two sorted arrays nums1 and nums2 of size m and n respectively.
 * Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).
 *
 * You may assume nums1 and nums2 cannot be both empty.
 */
class Problem4 extends TestCase
{
	/**
	 * Test Case
	 * Example:
	 * 		nums1 = [1, 3]
	 *		nums2 = [2]
	 *		The median is 2.0
	 * @param closure $solution
	 */
	private function case_1(closure $solution)
	{
		$numbersA = [1, 3];
		$numbersB = [2];
		$output = $solution($numbersA, $numbersB);
		$this->assertEquals(2.0, $output);
	}

	/**
	 * Test Case
	 * Example:
	 * 		nums1 = [1, 2]
	 *		nums2 = [3, 4]
	 *		The median is (2 + 3)/2 = 2.5
	 * @param closure $solution
	 */
	private function case_2(closure $solution)
	{
		$numbersA = [1, 2];
		$numbersB = [3, 4];
		$output = $solution($numbersA, $numbersB);
		$this->assertEquals(2.5, $output);
	}

	/**
	 * 公式が提供するテストケースはやや足りないと思いますが、多分配列を一つにまとめて中央の数字を取るかと思います。
	 * ポイントは恐らく時間複雑度は O(log (m+n))
	 * 一つの配列にまとめてから中央を計算する。
	 * @param array $numbersA
	 * @param array $numbersB
	 * @return float
	 * @throws Exception
	 */
	private function solution(array $numbersA, array $numbersB): float
	{
		$storageSpace = array();

		while (true) {
			if (count($numbersA) === 0 && count($numbersB) === 0) {
				break;
			}

			if (count($numbersA) === 0) {
				array_push($storageSpace, array_shift($numbersB));
				continue;
			}

			if (count($numbersB) === 0) {
				array_push($storageSpace, array_shift($numbersA));
				continue;
			}

			$number = $numbersA[0] < $numbersB[0] ? array_shift($numbersA) : array_shift($numbersB);
			array_push($storageSpace, $number);
		}

		$length = count($storageSpace);
		if ($length % 2 === 0) {
			return (float)($storageSpace[$length / 2] + $storageSpace[$length / 2 - 1]) / 2 ;
		}

		return $storageSpace[(int)floor($length / 2)];
	}

	/**
	 * @throws Exception
	 */
	public function test_run_solution(){
		$closure = function ($numbersA, $numbersB) {
			return $this->solution($numbersA, $numbersB);
		};

		$this->case_1($closure);
		$this->case_2($closure);
	}
}
