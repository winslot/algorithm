<?php namespace Tests\algorithm\leetcode;

use Closure;
use Exception;
use Tests\TestCase;
/**
 * Class Problem10:  Regular Expression Matching
 *
 * Given an input string (s) and a pattern (p), implement regular expression matching with support for '.' and '*'.
 *
 * '.' Matches any single character.
 * '*' Matches zero or more of the preceding element.
 *
 * The matching should cover the entire input string (not partial).
 *
 * ・ s could be empty and contains only lowercase letters a-z.
 * ・ p could be empty and contains only lowercase letters a-z, and characters like . or *.
 */
class Problem10 extends TestCase
{
	/**
	 * Test Case
	 * Example:
	 * 		Input:	s = "aa"
	 *				p = "a"
	 * 		Output: false
	 * Explanation: "a" does not match the entire string "aa".
	 * @param Closure $solution
	 */
	private function case_1(Closure $solution)
	{
		$string = "aa";
		$patten = "a";

		$output = $solution($string, $patten);

		$this->assertFalse($output);
	}

	/**
	 * Test Case
	 * Example:
	 * 		Input:	s = "aa"
	 * 				p = "a*"
	 * 		Output: true
	 * Explanation: '*' means zero or more of the preceding element, 'a'.
	 * 				Therefore, by repeating 'a' once, it becomes "aa".
	 * @param Closure $solution
	 */
	private function case_2(Closure $solution)
	{
		$string = "aa";
		$patten = "a*";

		$output = $solution($string, $patten);

		$this->assertTrue($output);
	}

	/**
	 * Test Case
	 * Example:
	 * 		Input:	s = "ab"
	 * 				p = ".*"
	 * 		Output: true
	 * Explanation: ".*" means "zero or more (*) of any character (.)".
	 * @param Closure $solution
	 */
	private function case_3(Closure $solution)
	{
		$string = "ab";
		$patten = ".*";

		$output = $solution($string, $patten);

		$this->assertTrue($output);
	}

	/**
	 * Test Case
	 * Example:
	 * 		Input:	s = "aab"
	 * 				p = "c*a*b"
	 * 		Output: true
	 * Explanation: c can be repeated 0 times, a can be repeated 1 time. Therefore, it matches "aab".
	 * @param Closure $solution
	 */
	private function case_4(Closure $solution)
	{
		$string = "aab";
		$patten = "c*a*b";

		$output = $solution($string, $patten);

		$this->assertTrue($output);
	}

	/**
	 * Test Case
	 * Example:
	 * 		Input:	s = "mississippi"
	 * 				p = "mis*is*p*."
	 * 		Output: false
	 * @param Closure $solution
	 */
	private function case_5(Closure $solution)
	{
		$string = "mississippi";
		$patten = "mis*is*p*.";
		$output = $solution($string, $patten);

		$this->assertFalse($output);
	}

	/**
	 * @param string $string
	 * @param string $patten
	 * @return bool
	 * @throws Exception
	 */
	private function solution(string $string, string $patten)
	{
		//コマンドを分割して検索していく
		// [m] [i] [s*] [i] [s*] [p*] [.]
		// [m]: mississippi => ississippi
		// [i]: ississippi => ssissippi
		// [s*]: ssissippi => issippi
		// [i]: issippi => ssippi
		// [s*]: ssippi => ippi
		// [p*]: ippi => not match
		// $cmds = $this->sliptcmd("mis*is*p*.");

		$cmds = $this->sliptcmd($patten);
		// $cmds = $this->combindCmds($cmds);
		foreach ($cmds as $cmd) {
			list($result, $string) = $this->applyCmd($cmd, $string);
			if($result == false) {
				return false;
			}
		}

		return $allMatch = strlen($string) == 0;
	}

	/**
	 * input $cmd 'm'
	 * input $string 'mississippi'
	 * output [$result, 'ississippi']
	 */
	private function applyCmd(string $cmd, string $string)
	{
		$shouldCutChars = strlen($cmd) != 1; // 'm' or 'm*'

		//m*
		if($shouldCutChars) {
			$keyword = $cmd[0];
			if($cmd == '.*') {
				return [true, ""];
			}

			while (strlen($string) > 0 && $string[0] == $keyword) {
				$string = substr($string, 1);
			}
			return [true, $string];
		}

		//m
		if (strlen($string)==0) {
			return [false, $string];
		}
		$keyword = $cmd[0];
		$isAnyKeyword = $keyword  == '.';
		if ($isAnyKeyword) {
			return [true, substr($string, 1)];
		}

		$isSuccess = $keyword === $string[0];
		return [$isSuccess, substr($string, 1)];
	}

	/**
	 * input:
	 *  string: sssa
	 *  keyword: s
	 * output:
	 *  int 4
	 *  [sssa, ssa, sa, a]
	 */
	private function chooseAmount(string $string, string $keyword)
	{
		$result = 1;
		$length = strlen($string);

		for($i = 0; $i<$length; $i++) {
			if($string[$i] != $keyword) {
				return $result;
			}

			$result++;
		}

		return $result;
	}

	private function sliptcmd(string $cmds)
	{
		// input "mis*is*p*."
		// output [m] [i] [s*] [i] [s*] [p*] [.]
		// $cmds = substr
		// $string = substr($cmds, 0, 2);
		$cmdArray = [];

		while(strlen($cmds) != 0 ) {
			list($cmd, $cmds) = $this->cutOneComand($cmds);
			$cmdArray[] = $cmd;
		}

		return $cmdArray;
	}

	/**
	 * input "mis*is*p*."
	 * output ['m', 'is*is*p*.']
	 */
	private function cutOneComand($cmds)
	{
		$shouldCutOneChar =  isset($cmds[1]) ? $cmds[1] != "*" : true;
		if ($shouldCutOneChar) {
			//['m', 'is*is*p*.']
			return [$cmds[0], substr($cmds, 1)];
		}

		//['m*', 'is*is*p*.']
		return [
			$cmds[0] . $cmds[1],
			substr($cmds, 2)
		];
	}

	/**
	 * @throws Exception
	 */
	public function test_run_solution(){
		$closure = function (string $input, string $patten) {
			return $this->solution($input, $patten);
		};

		$this->case_1($closure);
		$this->case_2($closure);
		$this->case_3($closure);
		$this->case_4($closure);
		$this->case_5($closure);
		// $this->freeCase($closure);
	}

	private function freeCase(Closure $solution)
	{
		//この実装方針では判断するのは困難であり、実装方針を考え直す
		$string = "aaa";
		$patten = "a*a";
		$output = $solution($string, $patten);

		$this->assertTrue($output);
	}

}
