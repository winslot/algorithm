<?php namespace Tests\algorithm\leetcode;

use Closure;
use Exception;
use Tests\TestCase;

/**
 * Class Problem6:  ZigZag Conversion
 *
 * The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this: (you may want to display this pattern in a fixed font for better legibility)
 */
class Problem6 extends TestCase
{
	/**
	 * Test Case
	 * Example:
	 * 		InputString: "PAYPALISHIRING"
	 * 		InputRow: "3"
	 *		Output: "PAHNAPLSIIGYIR"
	 *		Explanation:
	 * 			P   A   H   N
	 * 			A P L S I I G
	 * 			Y   I   R
	 * @param closure $solution
	 */
	private function case_1(closure $solution)
	{
		$inputString = "PAYPALISHIRING";
		$output = $solution($inputString, $row = 3);

		$this->assertTrue($output === "PAHNAPLSIIGYIR");
	}

	/**
	 * Test Case
	 * Example:
	 * 		InputString: "PAYPALISHIRING"
	 * 		InputRow: "4"
	 *		Output: "PINALSIGYAHRPI"
	 *		Explanation:
	 * 			P     I    N
	 * 			A   L S  I G
	 * 			Y A   H R
	 * 			P     I
	 * @param closure $solution
	 */
	private function case_2(closure $solution)
	{
		$inputString = "PAYPALISHIRING";
		$output = $solution($inputString, $row = 4);

		$this->assertTrue($output === "PINALSIGYAHRPI");
	}

	/**
	 * divide and conquer
	 * WWWをVで分ける、一つのVの長さが 2row - 2
	 *
	 * テストケースと同じように、第0~3行の列に文字を入れて答えを組み合わせる
	 *
	 *       左点    中点    右点
	 * o------◉------o------◉    << Vを平にする
	 * 行0   行1     行2    行1
	 *
	 * indexは最初点の位置
	 * 行0は最初の点を入れる	=>  string[index]
	 * 行2は中点を入れる		=>  合計行数 - 1 = 中点 (index0からので-1)
	 * 行1は左点と右点を入れる	=>  string[index+行数] = 左点
	 *						=>  中点 + 中点と左点の距離 = 右点
	 *
	 * Vの長さで上記のロジックをループすることで問題を解決する。
	 * for loopとrecursiveは交換できる
	 *
	 * @param string $input
	 * @param int $row
	 * @return string
	 */
	private function solution(string $input, int $row)
	{
		$stringLength = strlen($input);

		if ($row == 1) {
			return $input;
		}

		//WWWをVで分ける、長さは -> 2row - 2
		$lengthOfParts = (2 * $row) - 2;

		//答えを第１行〜N行で保存する予定のため、先に初期化をする
		$array = [];
		for ($i = 0; $i < $row; $i++){
			$array[$i] = "";
		}

		//Vの長さでループさせる。
		for ($i=0; $i < $stringLength; $i = $i + $lengthOfParts) {
			for ($j=0; $j < $row; $j++) {
				//0行目からN行目、 順番に数値を入れる
				//中央点以前の場合、N行 = 文字列の先頭Index+行数
				if ($j < $row) {
					$leftIndex = $i + $j;
					if ($leftIndex >= $stringLength) {
						continue;
					}
					$array[$j] = $array[$j] . $input[$leftIndex];
				}

				//最前行と最後行は重複しないため、ここで終わる。
				if ($j == 0 || $j == $row-1) {
					continue;
				}

				// i    lift    mid   right
				// o------◉------o------◉
				//中点は 先頭$i + 合計行数
				//左点は 先頭$i + 入れるの行数 $J
				//右点は 中点 + 中点と左点の距離
				$midPoint = $i + $row - 1;
				$leftPoint = $i + $j;
				$rightPoint = $midPoint + ($midPoint - $leftPoint);

				//文字列の長さが足りない場合は次の処理に移行
				if ($rightPoint >= $stringLength) {
					continue;
				}

				$array[$j] = $array[$j] . $input[$rightPoint];
			}
		}

		$result = array_reduce($array, function (string $accumulator, string $item) {
			return $accumulator . $item;
		}, "");

		return $result;
	}

	/**
	 * @throws Exception
	 */
	public function test_run_solution(){
		$closure = function (string $input, int $row) {
			return $this->solution($input, $row);
		};

		$this->case_1($closure);
		$this->case_2($closure);
	}
}
