<?php namespace Tests\algorithm\leetcode;

use Closure;
use Tests\TestCase;

/**
 * Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.
 *
 * Symbol       Value
 * I             1
 * V             5
 * X             10
 * L             50
 * C             100
 * D             500
 * M             1000
 * For example, 2 is written as II in Roman numeral, just two ones added together.
 * 12 is written as XII, which is simply X + II. The number 27 is written as XXVII, which is XX + V + II.
 *
 * 1 <= num <= 3999
 */
class Problem13 extends TestCase
{
    const VALUE_LIST = [
        'I' => 1,
        'IV' => 4,
        'V' => 5,
        'IX' => 9,
        'X' => 10,
        'XL' => 40,
        'L' => 50,
        'XC' => 90,
        'C' => 100,
        'CD' => 400,
        'D' => 500,
        'CM' => 900,
        'M' => 1000
    ];

    const SPECIAL_KEYWORD = ['IV', 'IX', 'XL', 'XC', 'CD', 'CM'];

	private function case_1(Closure $solution)
	{
		$input = "III";
		$output = $solution($input);
		$this->assertEquals("3", $output);
	}

	private function case_2(Closure $solution)
	{
		$input = "LVIII";
		$output = $solution($input);
		$this->assertEquals("58", $output);
	}

	private function case_3(Closure $solution)
	{
		$input = "MCMXCIV";
		$output = $solution($input);
		$this->assertEquals("1994", $output);
	}

	public function test_run_solution(){
		$closure = function (string $input) {
			return $this->solution($input);
		};

		$this->case_1($closure);
		$this->case_2($closure);
		$this->case_3($closure);
	}

    private function replaceString(string $string, string $needle):array
    {
        $position = strpos($string,$needle);
        if ($position !== false) {
            $newString = str_replace($needle, '', $string);
            return [$result = true, $newString];
        }

        return [false, $string];
    }

	private function solution(string $input): int
	{
        $sum = 0;
        foreach (self::SPECIAL_KEYWORD as $keyword) {
            list($result, $input) = $this->replaceString($input, $keyword);
            if ($result) {
                $sum = $sum + self::VALUE_LIST[$keyword];
            }
        }

        $len = strlen($input);
        for ($i=0; $i<$len; $i++) {
            $sum = $sum + self::VALUE_LIST[$input[$i]];
        }

        return $sum;
	}
}
