<?php namespace Tests\algorithm\leetcode;

use Closure;
use Tests\TestCase;

/**
 * Longest Common Prefix
 * Write a function to find the longest common prefix string amongst an array of strings.
 * If there is no common prefix, return an empty string "".
 *
 * Constraints:
 * 1 <= strs.length <= 200
 * 0 <= strs[i].length <= 200
 */
class Problem14 extends TestCase
{

	private function case_1(Closure $solution)
	{
		$input = ["flower","flow","flight"];
		$output = $solution($input);
		$this->assertEquals("fl", $output);
	}

	private function case_2(Closure $solution)
	{
		$input = ["dog","racecar","car"];
		$output = $solution($input);
		$this->assertEquals("", $output);
	}

    private function case_3(Closure $solution)
    {
        $input = ["flower","flower","flower","flower"];
        $output = $solution($input);
        $this->assertEquals("flower", $output);
    }


    public function test_run_solution(){
		$closure = function (array $input) {
			return $this->solution($input);
		};

		$this->case_1($closure);
		$this->case_2($closure);
		$this->case_3($closure);
	}

	private function solution(array $input): string
	{
        $result = "";
        $commonLength = 0;
        $inputCount = count($input);

        if ($inputCount === 1) {
            return $input[0];
        }

        foreach ($input as $string) {
            $commonLength = max($commonLength, strlen($string));
        }

        //char 0 1 2 3 4 ...    
        for ($i=0;$i<$commonLength;$i++) {
            $char = $input[0][$i];

            //string 1,2,3,4 ...
            for ($j=1; $j<$inputCount;$j++) {
                if ($input[$j][$i] !== $char) {
                    return $result;
                }
            }

            $result = $result . $char;
        }

        return $result;
	}
}




