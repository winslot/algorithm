<?php namespace Tests\algorithm;

use Tests\TestCase;

class UtilityServiceTest extends TestCase
{
    /**
     * @dataProvider binary_search_test_provider
     * @param array $numbers
     * @param int $need
     * @param bool $expect
     * @return void
     */
    public function test_binary_search(array $numbers, int $need, bool $expect)
    {
        $count = count($numbers);
        list($result, $index) = UtilityService::binarySearch($numbers, $need, 0, $count-1);
        $this->assertEquals($expect, $result);
    }

    public function binary_search_test_provider():array
    {
        return [
            [$numbers=[1], $need = 1, $expect= true],
            [[1], 0, false],
            [[1,2,3,4,5,6,7,8,9],0 ,false],
            [[1,2,3,4,5,6,7,8,9],1 ,true]
        ];
    }

    /**
     * @dataProvider quicksort_test_provider
     * @param array $input
     * @return void
     */
    public function test_quicksort(array $input)
    {
        $expect = $input;
        sort($expect);
        $this->assertNotEquals($input, $expect);

        UtilityService::quickSort($input, 0, count($input)-1);
        $this->assertEquals($expect, $input); //PHP公式のソート結果と一致する
    }

    public function quicksort_test_provider():array
    {
        return [
            [[8,3,6,4,5,7,2,1,7]],
            [[1,2,3,4,1,2,3,4,1,2,3,4,1,2,3,4]],
            [[3,54,5,6,33,7,34,4,116,8591,323,4567,23578,453434,8,734,423,5,3,4,2,1235]],
            [[3,0,-2,-1,1,2]]
        ];
    }
}
