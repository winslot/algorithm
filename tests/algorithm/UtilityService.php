<?php namespace Tests\algorithm;

class UtilityService
{
    public static function binarySearch(array $numbers, int $need, int $leftIndex, int $rightIndex): array
    {
        $distance = $rightIndex - $leftIndex;
        if ($distance <= 1) {
            if ($numbers[$leftIndex] === $need) {
                return [$result = true, $index = $leftIndex];
            }

            if ($numbers[$rightIndex] === $need) {
                return [$result = true, $index = $rightIndex];
            }

            return [$result = false, $index = -1];
        }

        //25 = 20 + (30 - 20)/2 切り上げ
        $middleIndex = $leftIndex + ceil($distance/2);
        $middle = $numbers[$middleIndex];
        if ($middle == $need) {
            return [$result = true, $index = $middleIndex];
        }

        if ($middle > $need) {
            return self::binarySearch($numbers, $need, $leftIndex, $middleIndex-1);
        }

        return self::binarySearch($numbers, $need, $middleIndex+1, $rightIndex);
    }

    public static function quickSort(array &$numbers, int $leftIndex, int $rightIndex)
    {
        //一個しかない場合そのままリターンする
        if ($rightIndex - $leftIndex < 1) {
            return;
        }

        //二個がある場合は交換する
        if ($rightIndex - $leftIndex == 1) {
            $isLessThan = $numbers[$leftIndex] < $numbers[$rightIndex];
            if (!$isLessThan) {
                self::swap($numbers, $leftIndex, $rightIndex);
            }
            return;
        }

        $from = $leftIndex;
        $to = $rightIndex-1;
        $pivotIndex = $rightIndex;
        for ($swapIndex = $from; $swapIndex <= $to; $swapIndex++) {

            //全スキャンし場合、最後はpivotとラストナンバーとの確認をする
            if ($swapIndex === $to) {
                $isLessThanPivot = $numbers[$swapIndex] < $numbers[$pivotIndex];
                if ($isLessThanPivot) {
                    break;
                }
                $newPivotIndex = $swapIndex;
                self::swap($numbers, $newPivotIndex, $pivotIndex);
                $pivotIndex = $newPivotIndex;
            }

            for ($searchIndex = $swapIndex+1; $searchIndex <= $to; $searchIndex++) {
                if ($numbers[$searchIndex] < $numbers[$pivotIndex]) {
                    self::swap($numbers, $swapIndex, $searchIndex);
                    break;
                }

                //全検索したが、pivotIndexより小さいのはない
                if ($searchIndex == $to) {
                    $isLessThanPivot = $numbers[$swapIndex] < $numbers[$pivotIndex];
                    $newPivotIndex = $isLessThanPivot ? $swapIndex+1 : $swapIndex;
                    self::swap($numbers, $newPivotIndex, $pivotIndex);
                    $pivotIndex = $newPivotIndex;
                    $swapIndex = $to;//swapを完了にする
                }

            }
        }

//      |3|4|6|7=>Pivot|9|8|
        self::quickSort($numbers, $leftIndex, $pivotIndex-1);
        self::quickSort($numbers, $pivotIndex+1, $rightIndex);
    }

    private static function swap(array &$numbers, int $indexA, int $indexB)
    {
        $tmp = $numbers[$indexA];
        $numbers[$indexA] = $numbers[$indexB];
        $numbers[$indexB] = $tmp;
    }
}
