<?php namespace Tests\algorithm\leetcode\algorithm\atCoder\AtCoderBeginnerContest246;

use Closure;

class FourPoints extends \Tests\algorithm\leetcode\TestCase
{
    private function solution(array $points):array
    {
        foreach ($points as $index => $point) {
            $otherPoints = array_filter($points, function (array $pointInArray) use ($point) {
                return $pointInArray !== $point;
            });
            $otherPoints = array_values($otherPoints);
            if ($point[0] == $otherPoints[0][0] && $point[1] == $otherPoints[1][1]) {
                return [$otherPoints[1][0], $otherPoints[0][1]];
            }

            if ($point[0] == $otherPoints[1][0] && $point[1] == $otherPoints[0][1]) {
                return [$otherPoints[0][0], $otherPoints[1][1]];
            }
        }

        return [0, 0];
    }

    private function case_1(Closure $closure)
    {
        $input = [
            [-1, -1],
            [-1, 2],
            [3, 2]
        ];

        $output = $closure($input);
        $this->assertEquals([3, -1], $output);
    }

    private function case_2(Closure $closure)
    {
        $input = [
            [-60, -40],
            [-60, -80],
            [-20, -80]
        ];

        $output = $closure($input);
        $this->assertEquals([-20, -40], $output);
    }

    public function test_run_solution(){
        $closure = function ($points) {
            return $this->solution($points);
        };

        $this->case_1($closure);
        $this->case_2($closure);
    }
}
